from django.shortcuts import render, render_to_response, get_object_or_404, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.db.models import Q, Count, IntegerField
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from multiprocessing import Value
from django.http import JsonResponse
from .models import Question, Answer, MyUser, Tag
from django.views.generic import TemplateView, View, FormView
from .forms import SentAnswerForm, LoginForm, NewQuestionForm
from .admin import UserCreationForm, UserChangeForm
from django.contrib.auth import authenticate, login, logout
import logging
from django.views.generic import FormView, RedirectView
from django.contrib.auth.decorators import login_required
import random
import datetime
from django.http import Http404
from django.contrib.auth.decorators import user_passes_test

# Create your views here.


def is_verified(user):
    if user.is_authenticated:
        try:
            user = MyUser.objects.get(nickname=user)
        except:
            user = None
    if user and user.is_verified:
       return True
    return False


def index(request):

    tags = Tag.objects.all()

    question_list_latest = Question.objects.latest().annotate(answers=Count('answer'))
    question_list_featured = Question.objects.popular().annotate(answers=Count('answer'))

    page_featured = request.GET.get('page_featured', 1)
    page_last = request.GET.get('page_last', 1)

    paginator_featured = Paginator(question_list_featured, 3) # Show 4 contacts per page
    paginator_last = Paginator(question_list_latest, 3)

    try:
        questions_featured = paginator_featured.page(page_featured)
        questions_last = paginator_last.page(page_last)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        questions_featured = paginator_featured.page(1)
        questions_last = paginator_last.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        questions_featured = paginator_featured.page(paginator_featured.num_pages)
        questions_last = paginator_last.page(paginator_last.num_pages)


    index_featured = questions_featured.number - 1
    max_index_featured = len(paginator_featured.page_range)
    start_index_featured = index_featured - 3 if index_featured >= 3 else 0
    end_index_featured = index_featured + 3 if index_featured <= max_index_featured - 3 else max_index_featured
    page_range_featured = list(paginator_featured.page_range)[start_index_featured:end_index_featured]

    index_last = questions_last.number - 1
    max_index_last = len(paginator_last.page_range)
    start_index_last = index_last - 3 if index_last >= 3 else 0
    end_index_last = index_last + 3 if index_last <= max_index_last - 3 else max_index_last
    page_range_last = list(paginator_last.page_range)[start_index_last:end_index_last]


    return render(request, 'questions/main.html', {"questions_featured": questions_featured,
                                                   "questions_last":questions_last,
                                                   "user": request.user,
                                                   "page_range_featured":page_range_featured,
                                                   "page_range_last":page_range_last,
                                                   "tags": tags})


def postLikeRedirect(request, *args, **kwargs):
    question_id = kwargs.get('id')
    obj = get_object_or_404(Question, id=question_id)
    user = request.user
    if user.is_authenticated:
        obj.toggle_like(user)
    return JsonResponse({"like": str(obj.total_likes)})


class ViewQuestion(TemplateView):

    template_name = 'questions/question_comments.html'

    def get_context_data(self, **kwargs):
        context = super(ViewQuestion, self).get_context_data(**kwargs)

        question = get_object_or_404(Question, id=self.kwargs['id'])
        answers = Answer.objects.filter(question__id=self.kwargs['id'])
        form = SentAnswerForm(self.request.POST or None)

        context['question'] = question
        context['answers'] = answers
        context['form'] = form
        context['tags'] = Tag.objects.all()
        # context['sended'] = str(datetime.datetime.now())

        return context

    def post(self, request, *args, **kwargs):
        context = self.get_context_data()

        if not context["form"].is_valid():
            return super(TemplateView, self).render_to_response(context)

        answer = Answer(text=context["form"].cleaned_data['text'], author=request.user, question=context['question'])
        answer.save()
        return HttpResponseRedirect(self.request.path_info)


@user_passes_test(is_verified, login_url='/', redirect_field_name=None)
def form_comment_submit(request, id):
    if request.method == 'POST':
        last_update = request.POST.get('sended')
        comment_text = request.POST.get('the_post')
        question = Question.objects.get(id=id)
        response_data = {}
        answer = Answer(text=comment_text, author=request.user, question=question)
        answer.save()
        new_answers = [{'text': ans.text, 'author':ans.author.avatar.url} for ans in Answer.objects.filter(added_at__gte=last_update)]
        response_data['answers'] = new_answers
        response_data['sended'] = str(datetime.datetime.now())
        return JsonResponse(response_data)
    else:
        return JsonResponse({'answers': [], 'sended':str(datetime.datetime.now())})

def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            user = authenticate(nickname=form.cleaned_data['nickname'], password=form.cleaned_data['password1'])
            login(request, user)
            return redirect('index')
    else:
        form = UserCreationForm()
    tags = Tag.objects.all()
    return render(request, 'questions/signup.html', {'form': form,
                                                     'tags': tags})


class LoginView(FormView):
    form_class = LoginForm
    template_name = 'questions/signin.html'
    success_url = ''

    def get_context_data(self, **kwargs):
        context = super(LoginView, self).get_context_data(**kwargs)
        context['tags'] = Tag.objects.all()
        return context

    def form_valid(self, form):
        user = authenticate(nickname=form.cleaned_data['nickname'], password=form.cleaned_data['password'])
        login(self.request, user)
        self.success_url = self.request.GET.get('next', '/')
        return redirect(self.success_url)


class AskQuestion(FormView):
    form_class = NewQuestionForm
    template_name = 'questions/ask.html'

    def get_context_data(self, **kwargs):
        context = super(AskQuestion, self).get_context_data(**kwargs)
        context['tags'] = Tag.objects.all()
        return context

    def form_valid(self, form):
        question = Question(title=form.cleaned_data['title'], text=form.cleaned_data['text'], author=self.request.user)
        question.save()
        return redirect('question_pathway', id=question.id)


class LogoutView(RedirectView):

    pattern_name = 'signin'

    def get_redirect_url(self, *args, **kwargs):

        if self.request.user.is_authenticated:
            logout(self.request)
        return super(LogoutView, self).get_redirect_url(*args, **kwargs)


def settings(request):
    user = request.user
    if request.method == 'POST':
        form = UserChangeForm(request.POST, request.FILES, instance=user)
        if form.is_valid():
            form.save()
        return redirect('index')
    else:
        form = UserChangeForm(instance=user)
        tags = Tag.objects.all()
        return render(request, 'questions/settings.html', {'form': form, 'user':request.user,
                                                           'tags': tags})


def tag_results(request, tag):
    question_list = Question.objects.filter(tags__name=tag).annotate(answers=Count('answer'))

    page = request.GET.get('page', 1)

    paginator = Paginator(question_list, 3)  # Show 4 contacts per page

    try:
        questions = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        questions = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        questions = paginator.page(paginator.num_pages)

    index= questions.number - 1
    max_index = len(paginator.page_range)
    start_index = index - 3 if index >= 3 else 0
    end_index = index + 3 if index <= max_index - 3 else max_index
    page_range = list(paginator.page_range)[start_index:end_index]

    tags = Tag.objects.all()

    return render(request, 'questions/results.html', {"questions": questions,
                                                      "user": request.user,
                                                      "page_range":page_range,
                                                      "tags": tags
                                                     })


def search_results(request, pattern):
    question_list = Question.objects.filter(Q(title__contains=pattern)|Q(text__contains=pattern)).annotate(answers=Count('answer'))

    page = request.GET.get('page', 1)

    paginator = Paginator(question_list, 3)  # Show 4 contacts per page

    try:
        questions = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        questions = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        questions = paginator.page(paginator.num_pages)

    index= questions.number - 1
    max_index = len(paginator.page_range)
    start_index = index - 3 if index >= 3 else 0
    end_index = index + 3 if index <= max_index - 3 else max_index
    page_range = list(paginator.page_range)[start_index:end_index]

    tags = Tag.objects.all()

    return render(request, 'questions/results.html', {"questions": questions,
                                                      "user": request.user,
                                                      "page_range":page_range,
                                                      "tags": tags
                                                     })


def verify(request, uuid):
    try:
        user = MyUser.objects.get(verification_uuid=uuid, is_verified=False)
    except:
        raise Http404("User does not exist or is already verified")

    user.is_verified = True
    user.save()
    user = authenticate(nickname=user.nickname, password=user.password)
    login(request, user, backend='django.contrib.auth.backends.ModelBackend')
    return redirect('index')
