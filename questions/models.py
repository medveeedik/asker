from django.db import models
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser
import uuid
from django.urls import reverse
from django.db.models import signals
from django.core.mail import send_mail
from .tasks import send_verification_email


class MyUserManager(BaseUserManager):
    def create_user(self, nickname, email, password=None):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        if not nickname:
            raise ValueError('Users must have a nickname')

        user = self.model(
            email=self.normalize_email(email),
            nickname=nickname,
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, nickname, email,  password):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_user(nickname=nickname, password=password, email=email)
        user.is_admin = True
        user.save(using=self._db)
        return user


class MyUser(AbstractBaseUser):
    email = models.EmailField(verbose_name='email address', max_length=255)
    nickname = models.CharField(max_length=15, unique=True)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
    avatar = models.ImageField(upload_to='static/images', default='anonim.png')
    is_verified = models.BooleanField('verified', default=False)
    verification_uuid = models.UUIDField('Unique Verification UUID', default=uuid.uuid4)

    objects = MyUserManager()

    USERNAME_FIELD = 'nickname'
    REQUIRED_FIELDS = ['email']

    def get_full_name(self):
        return self.nickname

    def get_short_name(self):
        return self.nickname

    def __unicode__(self):
        return self.nickname

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    @property
    def is_staff(self):
        return self.is_admin


def user_post_save(sender, instance, signal, *args, **kwargs):
    if not instance.is_verified:
        # Send verification email
        send_verification_email.delay(instance.pk)


signals.post_save.connect(user_post_save, sender=MyUser)


class QuestionManager(models.Manager):

    def latest(self, n=None):
        questions = self.order_by('-added_at')
        if n:
            return questions[:n]
        return questions

    def popular(self, n=None):
        questions = self.order_by('-rating')
        if n:
            return questions[:n]
        return questions


class Tag(models.Model):
    id = models.UUIDField(default=uuid.uuid4(), primary_key=True)
    name = models.CharField(max_length=15)

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name


class Question(models.Model):
    id = models.UUIDField(default=uuid.uuid4, primary_key=True)
    title = models.CharField(max_length=50)
    text = models.TextField()
    author = models.ForeignKey(MyUser, on_delete=models.CASCADE)
    added_at = models.DateTimeField(auto_now_add=True)
    rating = models.IntegerField(default=0)
    tags = models.ManyToManyField(Tag, blank=True)
    likes = models.ManyToManyField(MyUser, related_name='people_liked')

    @property
    def total_likes(self):
        return self.likes.count()

    def __unicode__(self):
        return self.title

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("question_pathway", kwargs={"id": self.id})

    def get_like_url(self):
        return reverse("like-toggle", kwargs={"id": self.id})

    objects = QuestionManager()

    def add_like(self, user):
        self.likes.add(user)
        return self.total_likes

    def dislike(self, user):
        self.likes.remove(user)
        return self.total_likes

    def toggle_like(self, user):
        if user in self.likes.all():
            self.likes.remove(user)
        else:
            self.likes.add(user)


class Answer(models.Model):
    id = models.UUIDField(default=uuid.uuid4, primary_key=True)
    text = models.TextField()
    author = models.ForeignKey(MyUser, on_delete=models.CASCADE)
    added_at = models.DateTimeField(auto_now_add=True)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)

