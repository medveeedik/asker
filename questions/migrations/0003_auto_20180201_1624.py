# Generated by Django 2.0.1 on 2018-02-01 16:24

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('questions', '0002_auto_20180201_1024'),
    ]

    operations = [
        migrations.AlterField(
            model_name='answer',
            name='id',
            field=models.UUIDField(default=uuid.UUID('82e77c6a-bad2-4d4a-bd65-30bfafaca29c'), primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='question',
            name='id',
            field=models.UUIDField(default=uuid.UUID('db71b2ce-f6a5-42ca-83a7-044aed943628'), primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='tag',
            name='id',
            field=models.UUIDField(default=uuid.UUID('61cb945e-bcd3-4a3e-9030-ad5d7ac891ac'), primary_key=True, serialize=False),
        ),
    ]
