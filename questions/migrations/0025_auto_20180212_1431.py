# Generated by Django 2.0.1 on 2018-02-12 14:31

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('questions', '0024_auto_20180212_1324'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tag',
            name='id',
            field=models.UUIDField(default=uuid.UUID('d521ed8b-db3f-4d80-ade0-41910d74b29d'), primary_key=True, serialize=False),
        ),
    ]
