# Generated by Django 2.0.1 on 2018-02-07 22:40

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('questions', '0013_auto_20180207_2150'),
    ]

    operations = [
        migrations.AlterField(
            model_name='myuser',
            name='avatar',
            field=models.ImageField(default='anonim.png', upload_to='static/images'),
        ),
        migrations.AlterField(
            model_name='tag',
            name='id',
            field=models.UUIDField(default=uuid.UUID('ecabbc2e-232c-4944-8497-85e0d4c7498b'), primary_key=True, serialize=False),
        ),
    ]
