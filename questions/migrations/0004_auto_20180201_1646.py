# Generated by Django 2.0.1 on 2018-02-01 16:46

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('questions', '0003_auto_20180201_1624'),
    ]

    operations = [
        migrations.AlterField(
            model_name='answer',
            name='id',
            field=models.UUIDField(default=uuid.UUID('1db55ea0-4d5b-4829-bcbd-f7878b2c83b1'), primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='question',
            name='id',
            field=models.UUIDField(default=uuid.UUID('a1a943af-6967-4900-b2ed-c2b171f1c94d'), primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='question',
            name='tags',
            field=models.ManyToManyField(null=True, to='questions.Tag'),
        ),
        migrations.AlterField(
            model_name='tag',
            name='id',
            field=models.UUIDField(default=uuid.UUID('d34a8736-64df-47c6-86b5-ce9a52e3a427'), primary_key=True, serialize=False),
        ),
    ]
