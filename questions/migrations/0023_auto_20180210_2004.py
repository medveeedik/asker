# Generated by Django 2.0.1 on 2018-02-10 20:04

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('questions', '0022_auto_20180210_2000'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tag',
            name='id',
            field=models.UUIDField(default=uuid.UUID('dc44da7d-6291-4654-ad3c-fa0e6f2544ae'), primary_key=True, serialize=False),
        ),
    ]
