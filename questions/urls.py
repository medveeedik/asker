from django.conf.urls import url, include
from django.contrib.auth.decorators import login_required
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^ask_url$', login_required(views.AskQuestion.as_view(), login_url='signin?next={{ request.path }}'), name='ask'),
    url(r'^signin$', views.LoginView.as_view(), name='signin'),
    url(r'^logout$', views.LogoutView.as_view(), name='logout'),
    url(r'^signup$', views.signup, name='signup'),
    url(r'^settings$', views.settings, name='settings'),
    url(r'^search_results/(?P<pattern>[\w\-]*)/$$', views.search_results, name='search_results'),
    url(r'^tag_results/(?P<tag>[\w\-]+)/$', views.tag_results, name='tag_results'),
    url(r'^question/(?P<id>[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12})/likes$', views.postLikeRedirect, name='like-toggle'),
    url(r'^question/(?P<id>[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12})/add_comment', views.form_comment_submit, name='add_comment'),
    url(r'^question/(?P<id>[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12})/$', views.ViewQuestion.as_view(), name='question_pathway'),
    url(r'^verify/(?P<uuid>[a-z0-9\-]+)/', views.verify, name='verify'),
]
