from .models import MyUser
import logging


class MyAuthBackend(object):

    def authenticate(self, nickname, password):
        try:
            user = MyUser.objects.get(nickname=nickname)
            if user.check_password(password):
                return user
            else:
                return None
        except Exception as e:
            return None

    def get_user(self, user_id):
        try:
            user = MyUser.objects.get(pk=user_id)
            if user.is_active:
                return user
            return None
        except Exception as e:
            return None
