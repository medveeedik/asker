from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.forms import AuthenticationForm
from .models import MyUser
from django.core.exceptions import ValidationError
from .models import MyUser, Question
import datetime


class SentAnswerForm(forms.Form):
    text = forms.CharField( min_length=1,widget=forms.Textarea(attrs={'placeholder': 'Type here your answer',
                                                                      'id':'post-text'}))
    sended = forms.CharField(widget=forms.HiddenInput(attrs={'id':'post-sended',
                                                             'value': str(datetime.datetime.now())}))


class LoginForm(forms.Form):
    nickname = forms.CharField(label="Username", max_length=30,
                               widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'nickname',
                                                             'placeholder': 'Your nickname'}))
    password = forms.CharField(label="Password", max_length=30,
                               widget=forms.PasswordInput(attrs={'class': 'form-control', 'name': 'password',
                                                                 'placeholder': 'Password'}))

    def clean(self):
        cleaned_data = super(LoginForm, self).clean()

        try:
            user = MyUser.objects.get(nickname=cleaned_data.get("nickname"))
        except Exception as e:
            raise forms.ValidationError("No such user!!")
        if not user.check_password(raw_password=cleaned_data.get("password")):
            raise forms.ValidationError("Wrong password!")


class NewQuestionForm(forms.Form):
    title = forms.CharField(label='Title', max_length=50, min_length=3,
                            widget=forms.TextInput(attrs={'placeholder': 'Title', 'class':'question-title'}))

    text = forms.CharField(label='Text', min_length=3,
                           widget=forms.Textarea(attrs={'placeholder': 'Text', 'class':'question-text'}))

    def clean_title(self):
        data = self.cleaned_data['title']
        if Question.objects.filter(title=data).exists():
            raise forms.ValidationError("Question with such title already exists!")
        return data
